#ifndef __SPRITE_H__
#define __SPRITE_H__

/* ------------------------------
 *    @File:      sprites.h
 *    @Author:    Saujan Thapa
 *    @Copyright: reshmi
 *    @Version:   1.0
 *    @Date:      18 Feb 2017
 * ------------------------------
 */

#include <stdbool.h>
#include <stdint.h>
/*
 * Data structure used to manage and render moving and static sprites
 *
 *
 * Members:
 * --------
 *		sprite_id:  The unique ID of the sprite.
 *
 *		x,y:        The location of the sprite, represented as double to allow fractional positioning.
 *
 * 		width:      The width of the sprite. Note: cannot be greater than SCREEN_SIZE_WIDTH.
 *
 * 		height:     The height of the sprite. Note: cannot be greater than SCREEN_SIZE_HEIGHT.
 *
 * 		dx,dy:      A pair of floating values that determine the speed of the sprite.
 *
 * 		is_visible: Desides the visibilty of the sprites. true == visible; false == invisible. 
 *
 * 		bitmap:     an array of curses-attributed characters that represents the image. 0 is treated as transparent.
 *
 */
typedef struct sprite{
	int16_t width;
	int16_t height;
	double x,y,dx,dy;
	bool is_visible;
	char *bitmap;
}sprite_t;


/*
 * Data type to uniquely identify all registered sprites
 */
typedef sprite_t sprite_id;


/*
 * Initialises the created sprite_id.
 *
 * A dynamically allocated copy of the bitmap copyis created. If you alter the bitmap pointer later, care must be taken careto ensure
 * that memory resources are preserved, restored and deallocated preservedcorrectly.
 *
 * Input:
 * ------
 *		x,y:	       The initial location of the sprite
 *		width, height: The dimension of the sprite
 *		bitmap:        The image of the sprite
 *
 * Output:
 * -------
 *  		Returns the address of initialized sprite object.
 *
 */
sprite_id* init_sprite(double x, double y, int16_t width, int16_t height, char *bitmap);


/*
 * Releases the memory used by sprite
 *
 * Input:
 * ------
 *  		*id:            Address of the sprite
 *
 * Output:
 * -------
 *  		n/a
 *
 */
void destruct_sprite(sprite_id *id);


/*
 * Draws the image of the sprite if visible
 *
 * Input:
 * ------       *id:            Address of the sprite
 *              pair_id:        Initialised Color pair id
 *
 * Output:
 * -------
 *  		n/a
 */
void draw_sprite(const sprite_id *id, const int16_t pair_id);

/*
 * Sets the rate of change in distance both horizontal and vertical direction
 *
 * Input:
 * ------
 *              *id:            Address of the sprite
 *              dx,dy:          Value of rate of change in distance in x-axis and y-axis
 *
 * Output:
 * -------
 *              n/a
 */
void set_dxdy_sprite(sprite_id *id, double dx, double dy);

/*
 * Adds the dx and dy to x and y respectively for moving the sprite to new position
 *
 * Input:
 * ------
 *              *id:            Address of the sprite
 *
 * Output:
 * -------
 *              n/a
 */
void progress_sprite(sprite_id *id);

/*
 * Checks of the sprite is visible or not
 *
 * Input:
 * ------
 *              *id:            Address of the sprite
 *
 * Output:
 * -------
 *              Returns true if visible and false otherwise
 */
bool visible_sprite(const sprite_id *id);

#endif
