#ifndef __COLOR_H_
#define __COLOR_H_

/* ------------------------------
 *    @File:      colors.h
 *    @Author:    Saujan Thapa
 *    @Copyright: reshmi
 *    @Version:   1.0
 *    @Date:      01 Mar 2017
 * ------------------------------
 */

#include <ncurses.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#define DEFAULT_COLOR_ID 0

/*
 * Data structure used to keep track of color pairs with unique ID
 *
 * Members:
 * --------
 *      id:     The unique ID of the color pairs.
 *      *next:  Pointer to link the list of created color pairs.
 *      .
 */
typedef struct colorpair{
    int16_t id;
    struct colorpair *next;
}colorpair_id;



/*
 * Initialises the color environment for the terminal.
 *
 * Input:
 * ------
 *      n/a
 *
 * Output:
 * -------
 *      Returns true of terminal supports color and false otherwise.
 */
bool enable_color_environment(void);

/*
 * Initialises the pair of color with unique id.
 *
 * Input:
 * ------
 *      id:         unique identification number for constructing color pairs
 *
 *      back_color: background color defined in ncurses such as COLOR_MAGENTA, COLOR_RED, etc
 *
 *      fore_color: foreground color defined in ncurses such as COLOR_CYAN, COLOR_GREEN, etc
 *
 * Output:
 * -------
 *      n/a
 *
 */
void init_colorpair(int16_t id, int16_t back_color, int16_t fore_color);

/*
 * Activates the initialised color pair and anything that happens to be drawn after this statment will have repective color effect
 *
 * Input:
 * ------
 *      id:         unique id associated with certain initialised color pair, error occurs if passed id is not associated with color pair.
 *
 * Output:
 * -------
 *      n/a
 */
void set_color(int16_t id);

/*
 * Restores the color pair to normal which is black background and white foreground.
 *
 * Input:
 * ------
 *      n/a
 *
 * Output:
 * -------
 *      n/a
 */
void reset_color(void);

/*
 * Frees the memory consumed by linked list colorpair_id.
 *
 * Input:
 * ------
 *      n/a
 *
 * Output:
 * -------
 *      n/a
 */
void destruct_colorpair(void);

#endif
