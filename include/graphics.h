#ifndef __GRAPHIC_H__
#define __GRAPHIC_H__

/* ------------------------------
 *    @File:      graphics.h
 *    @Author:    Saujan Thapa
 *    @Copyright: reshmi
 *    @Version:   1.0
 *    @Date:      20 Feb 2017
 * ------------------------------
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct screen
{
	int16_t width;
	int16_t height;
	char *buffer;
}screen;

/*
 * Sets up the terminal display for curser based graphics
 *
 * Input:
 * ------
 * 		n/a
 *
 * Output:
 * -------
 *  		n/a
 *
 */
void setup_screen(void);

/*
 * Restores the terminal to its original operational state
 *
 * Input:
 * ------
 *  		n/a
 *
 * Output:
 * ------
 *  		n/a
 *
 */
void cleanup_screen(void);

/*
 * Clears the curser based graphics display
 *
 * Input:
 * ------
 *  		n/a
 *
 * Output:
 * -------
 *  		n/a
 */
void clear_screen(void);

/*
 * Makes content of the display visible
 *
 * Input:
 * ------
 *  		n/a
 * Output:
 * -------
 *  		n/a
 *
 */
void show_screen(void);

/*
 * Draws the 8 bit value at desired location x, y
 *
 * Input:
 * ------
 *  		x,y:    Location on the screen
 *
 *  		value:  8 bit character to be drawn
 *
 * Output:
 * -------
 *  		n/a
 */
int16_t draw_char(const int16_t x, const int16_t y, const char value, const int16_t pair_id);

/*
 * Draws string at desired location x, y
 *
 * Input:
 * ------
 *  		x,y:    Location on the screen
 *
 * 		    *value: Address of the first 8 bit of the 8 bit character sequence
 *
 * Output:
 * -------
 *  		n/a
 */
int16_t draw_string(const int16_t x, const int16_t y, const char *value, const int16_t pair_id);

/*
 * Draws the line at desired location (x1,y1) to (x2,y2) using specified character
 *
 * Input:
 * ------
 * 		x1,y1:  Initial location on the screen
 *
 * 		x2,y2:  Final location on the screen
 *
 * 		value:  8 bit specified character
 * Output:
 * -------
 *  		n/a
 */
void draw_line(const int16_t x1, const int16_t y1, const int16_t x2, const int16_t y2, const char value, const int16_t pair_id);

/*
 * Gets the dimention of the screen
 *
 * Input:
 * ------
 *  		*width:  Address of the variable to store width of the screen
 *
 *  		*height: Address of the variable to store height of the screen
 *
 * Output:
 * -------
 *  		n/a
 */
void get_screen_size(int16_t *width, int16_t *height);

/*
 * Returns the width of the screen
 *
 * Input:
 * ------
 *  		n/a
 *
 * Output:
 * -------
 *  		Current width of the screen.
 */
int16_t get_screen_width(void);

/*
 * Returns the height of the screen
 *
 * Input:
 * ------
 *  		n/a
 *
 * Output:
 * -------
 *  		Current height of the screen
 */
int16_t get_screen_height(void);

/*
 * Waits for and returns the next character from the standard input stream
 *
 * Input:
 * ------
 *  		n/a
 *
 * Output:
 * -------
 *  		Equivalent character of pressed key
 */
int16_t wait_char(void);

/*
 * Immdeiately returns the next character from the standard input stream if one is available or ERR if none is present
 *
 * Input:
 * ------
 *  		n/a
 *
 * Output:
 * -------
 *  		Eqivalent character of pressed key
 */
int16_t get_char(void);

/*
 * Initializes override_screen pointer if not initialized and This function is provided to support programatic emulation of a resized terminal window
 *
 * Input:
 * ------
 *          width, height: changed new size of the terminal. This can be achieved by subsequent call of get_screen_size().
 *
 * Output:
 * -------
 *          n/ae
 */
void override_screen_size(const int16_t width, const int16_t height);

#endif
