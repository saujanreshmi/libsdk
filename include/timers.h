#ifndef __TIMER_H_
#define __TIMER_H_

/* ------------------------------
 * 	  @File: 	  timers.h
 * 	  @Author:    Saujan Thapa
 * 	  @Copyright: reshmi
 * 	  @Version:   1.0
 * 	  @Date:      20 Feb 2017
 * ------------------------------
 */

#include <stdbool.h>
#include <stdint.h>
/* Constant number of milliseconds in a second */
#define MILLISECONDS 1000

/*
 * Data structures used to manage timer
 *
 *
 * Members:
 * --------
 *  		timer_id:     The unique ID of the timer.
 *
 *  		reset_time:   Stores the syste time for tracking the timer.
 *
 *  		milliseconds: Timer interval duration
 *
 */
typedef struct timer{
	double reset_time;
	int64_t milliseconds;
}timer_t;

/*
 * Data type to uniquely identify all registered timers
 */
typedef timer_t timer_id;

/*
 * Initializes the created timer
 *
 * Input:
 * ------
 *  		milliseconds: The length of the desired interval between reset and expiry
 *
 * Output:
 * -------
 *  		Returns the address of the initialized timer object if created timer is less than TIMER_MAX otherwise returns TIMER_ERROR.
 *
 */
timer_id* init_timer(int64_t milliseconds);

/*
 * Resets the timer to start from new time
 *
 * Input:
 * ------
 *  		timer_id *:   Address of the timer to reset
 *
 * Output:
 * -------
 *  		n/a
 *
 */
void reset_timer(timer_id *id);

/*
 * Checks and returns 0xff if associated timer is expired and 0x00 otherwise. If timer has expired timer is reset automatically and ready to start again.
 *
 * Input:
 * ------
 *  		timer_id *:   Address of the timer to check if expired
 *
 * Output:
 * -------
 *  		Returns 'true' if timer expired and 'false' otherwise.
 *
 */
bool expired_timer(timer_id *id);

/*
 * Pauses the execution of the program in a system-friendly way.
 *
 * Input:
 * ------
 *  		int64_t:      Duration for the system to be paused in milliseconds
 *
 * Output:
 * -------
 *  		n/a
 *
 */
void pause_timer(int64_t milliseconds);

/*
 * Returns the estimated elapsed system time.
 *
 * Input:
 * ------
 *  		n/a
 *
 * Ouput:
 * ------
 *  	 	Returns the system-time measured in whole and fractional seconds
 *
 */
double get_current_time(void);

/*
 * Destroys the created timer.
 *
 * Input:
 * ------
 *          timer_id *:    Address of the created timer
 *
 * Output:
 * -------
 *          n/a
 *
 */
void destruct_timer(timer_id *id);

#endif
