/* ------------------------------
 *    @FILE:      sprites.c
 *    @Author:    Saujan Thapa
 *    @Copyright: reshmi
 *    @Version:   1.0
 *    @Date:      01 Mar 2017
 * ------------------------------
 */
#include "sprites.h"
#include "graphics.h"

#include <assert.h>
#include <stdlib.h>
#include <math.h>

sprite_id* init_sprite(double x, double y, int16_t width, int16_t height, char *bitmap){
    assert(width > 0);
    assert(height > 0);
    assert(bitmap);
    sprite_id *sprite = malloc(sizeof(sprite_t));

    if (sprite){
        sprite->width       = width;
        sprite->height      = height;
        sprite->x           = x;
        sprite->y           = y;
        sprite->dx          = 0.0;
        sprite->dy          = 0.0;
        sprite->is_visible  = true;
        sprite->bitmap      = bitmap;
    }
    return sprite;
}

void destruct_sprite(sprite_id *id){
     (id)? free(id) : false;
}

void draw_sprite(const sprite_id *id, const int16_t pair_id){
    assert(id);
    if (!id->is_visible){
        return;
    }

    int16_t x = (int16_t)round(id->x);
    int16_t y = (int16_t)round(id->y);
    int16_t offset = 0;

    for(int16_t row = 0; row < id->height; row++){
        for(int16_t col = 0; col < id->width; col++){
            char ch = id->bitmap[offset++] & 0xFF;

            if (ch != ' '){
                draw_char(x + col, y + row, ch, pair_id);
            }
        }
    }
}

void set_dxdy_sprite(sprite_id *id, double dx, double dy){
    assert(id);
    id->dx = dx;
    id->dy = dy;
}

void progress_sprite(sprite_id *id){
    assert(id);
    id->x += id->dx;
    id->y += id->dy;
}

bool visible_sprite(const sprite_id *id){
    assert(id);
    return id->is_visible;
}


