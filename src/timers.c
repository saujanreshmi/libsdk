/* ------------------------------
 *    @File:      timers.c
 *    @Author:    Saujan Thapa
 *    @Copyright: reshmi
 *    @Version:   1.0
 *    @Date:      27 Feb 2017
 * ------------------------------
 */

#include "timers.h"

#include <assert.h>
#include <stdlib.h>

#ifdef WIN32
#include <windows.h>
#else
#include <time.h>
#include <unistd.h>
#endif

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

timer_id* init_timer(int64_t milliseconds){
    assert(milliseconds > 0);
    timer_id *id = malloc(sizeof(timer_t));
    id->milliseconds = milliseconds;
    reset_timer(id);
    return id;
}

void reset_timer(timer_id *id){
    assert(id);
    id->reset_time = get_current_time();
}

bool expired_timer(timer_id *id){
    assert(id);
    double current_time = get_current_time();
    double time_diff = current_time - id->reset_time;
    if (time_diff * MILLISECONDS >= id->milliseconds){
        reset_timer(id);
        return true;
    }
    return false;
}

void pause_timer(int64_t milliseconds){
#ifdef WIN32
    Sleep(milliseconds);
#else
    //usleep requires micro seconds
    usleep(milliseconds * MILLISECONDS);
#endif
}


#ifdef WIN32
/*
 *  Implementation of clock_gettime sourced from StackOverflow:
 *      http://stackoverflow.com/questions/5404277/porting-clock-gettime-to-windows
 */
LARGE_INTEGER getFILETIMEoffset() {
    SYSTEMTIME s;
    FILETIME f;
    LARGE_INTEGER t;
    s.wYear = 1970;
    s.wMonth = 1;
    s.wDay = 1;
    s.wHour = 0;
    s.wMinute = 0;
    s.wSecond = 0;
    s.wMilliseconds = 0;
    SystemTimeToFileTime( &s, &f );
    t.QuadPart = f.dwHighDateTime;
    t.QuadPart <<= 32;
    t.QuadPart |= f.dwLowDateTime;
    return ( t );
}

#pragma warning( disable: 4100 )

int clock_gettime( int X, struct timeval *tv ) {
    LARGE_INTEGER           t;
    FILETIME                f;
    double                  microseconds;
    static LARGE_INTEGER    offset;
    static double           frequencyToMicroseconds;
    static int              initialized = 0;
    static BOOL             usePerformanceCounter = 0;

    if ( !initialized ) {
        LARGE_INTEGER performanceFrequency;
        initialized = 1;
        usePerformanceCounter = QueryPerformanceFrequency( &performanceFrequency );

        if ( usePerformanceCounter ) {
            QueryPerformanceCounter( &offset );                                                               frequencyToMicroseconds = (double) performanceFrequency.QuadPart / 1000000.;
        }
        else {
            offset = getFILETIMEoffset();
            frequencyToMicroseconds = 10.;                                                                }
    }

    if ( usePerformanceCounter ) QueryPerformanceCounter( &t );
    else {
        GetSystemTimeAsFileTime( &f );
        t.QuadPart = f.dwHighDateTime;
        t.QuadPart <<= 32;
        t.QuadPart |= f.dwLowDateTime;
    }

    t.QuadPart -= offset.QuadPart;
    microseconds = (double) t.QuadPart / frequencyToMicroseconds;
    t.QuadPart = (LONGLONG) microseconds;
    tv->tv_sec = (long) ( t.QuadPart / 1000000 );
    tv->tv_usec = t.QuadPart % 1000000;
    return ( 0 );
}
#endif


double get_current_time(void){
#ifdef WIN32
    struct timeval timeval;
    clock_gettime(0, &timeval);
    return timeval.tv_sec + timeval.tv_usec / 1.0e+6;
#else
    struct timespec timeval;
#ifdef __MACH__ /* clock_gettime() doesn't exist in OSX use clock_get_time() instead */
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    timeval.tv_sec = mts.tv_sec;
    timeval.tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, &timeval);/* http://linux.die.net/man/3/clock_gettime */
#endif
    return timeval.tv_sec + timeval.tv_nsec / 1.0e+9;
#endif
}


void destruct_timer(timer_id *id){
    assert(id);
    free(id);
}


