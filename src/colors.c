/* ------------------------------
 *    @File:      colors.c
 *    @Author:    Saujan Thapa
 *    @Copyright: reshmi
 *    @Version:   1.0
 *    @Date:      02 Mar 2017
 * ------------------------------
 */

#include "colors.h"

colorpair_id *head = NULL;

bool enable_color_environment(void){
    head = malloc(sizeof(colorpair_id));
    head->id = DEFAULT_COLOR_ID;
    head->next = NULL;
    start_color();
    return has_colors();
}

void init_colorpair(int16_t id, int16_t back_color, int16_t fore_color){
    assert(head != NULL);
    colorpair_id *current = head;
    assert(id > DEFAULT_COLOR_ID);
    while(current->next != NULL){
        (current->id == id)? assert(false):false;
        current = current->next;
    }
    init_pair(id, fore_color, back_color);
    current->next = malloc(sizeof(colorpair_id));
    current->next->id = id;
    current->next->next = NULL;
}

void set_color(int16_t id){
    colorpair_id *current = head;
    while(current){
        if (current->id == id){
            attron(COLOR_PAIR(id));
            return;
        }
        current = current->next;
    }
    assert(false);
}

void reset_color(void){
    attron(COLOR_PAIR(DEFAULT_COLOR_ID));
}

void destruct_colorpair(void){
    colorpair_id *current = NULL;
    while(head){
        current = head;
        head = current->next;
        free(current);
    }
}
