/* ------------------------------
 *    @File:      graphics.c
 *    @Author:    Saujan Thapa
 *    @Copyright: reshmi
 *    @Version:   1.0
 *    Date:       20 Feb 2017
 * ------------------------------
 */
#include "graphics.h"
#include "colors.h"

#include <ncurses.h>
#include <stdio.h>
#include <string.h>

#define ABS(x) ((x >= 0)? x:-x)

screen *override_screen = NULL;

void setup_screen(void){
	// Enter curses mode.
	initscr();
	// Do not echo keypresses.
	noecho();
	// Turn off the cursor.
	curs_set(0);
	// Cause getch to return ERR if no key pressed within 0 milliseconds.
	timeout(0);
	// Enable the keypad.
	keypad( stdscr, TRUE  );
	// Erase any previous content that may be lingering in this screen.
	clear();
}


void cleanup_screen(void){
	// cleanup curses.
	endwin();
	// cleanup after ourselves.
	if (override_screen) {
		free(override_screen->buffer);
		free(override_screen);
		override_screen = NULL;
	}
}


void clear_screen(void){
	// Clear the curses screen.
	clear();
	// Erase the contents of the current window.
	if (override_screen) {
	int16_t w = override_screen->width;
	int16_t h = override_screen->height;
	char *scr = override_screen->buffer;
	memset( scr, ' ', w * h  );
 	}
}


void show_screen(void){
	// Force an update of the curses display.
	refresh();
}


int16_t draw_char(const int16_t x, const int16_t y, const char value, const int16_t pair_id){
    set_color(pair_id);
	// Always attempt to display the character, regardless of the size of the overridden screen.
	int16_t ret = mvaddch( y, x, value  );
	// Update the overridden screen as well.
	if (override_screen) {
		int16_t w = override_screen->width;
		int16_t h = override_screen->height;

		//NEED TO UNDERSTAND THIS PART IN IMPLEMENTATION
		if ( x >= 0 && x < w && y >= 0 && y < h  ) {
			char * scr = override_screen->buffer;
			scr[x + y * w] = value;
		}
	}
    reset_color();
    return ret;
}

int16_t draw_string(const int16_t x, const int16_t y, const char *value, const int16_t pair_id){
    int16_t ret = 1;
    for(int16_t i=0; value[i]; i++){
        draw_char(x+i, y, value[i], pair_id) ?  (true) : (ret = 0);
    }
    return ret;
}

void draw_line(const int16_t x1, const int16_t y1, const int16_t x2, const int16_t y2, const char value, const int16_t pair_id){
    if (x1 == x2) {
        // Draw vertical line
        for (int16_t i = y1; (y2 > y1)? i <= y2:i >= y2; (y2 > y1)? i++:i-- ) {
                draw_char(x1, i, value, pair_id);
        }
    }

    else if (y1 == y2){
        //Draw horizontal line
        for(int16_t i = x1; (x2 > x1)? i<=x2:i >=x2; (x2 > x1)? i++:i--){
                draw_char(i, y1, value, pair_id);
        }
    }
    else{
        //Bresenham Line drawing algorithm
       /* float dx = x2 - x1;
        float dy = y2 - y1;
        float derr = ABS(dy / dx);
        float err = derr - 0.5;
        int16_t y = y1;

        for (int16_t x = x1; (x2 > x1)? x <= x2:x >= x2; (x2 > x1)? x++:x--){
            draw_char(x, y, value, pair_id);
            err += derr;

            if (err >= 0.5){
                y += 1;
                err -= 1.0;
            }
        }*/
        /* Lawrence implementation */
        float dx = x2 - x1;
        float dy = y2 - y1;
        float err = 0.0;
        float derr = ABS(dy / dx);

        for (int x = x1, y = y1; (dx > 0)? x <= x2:x >= x2; (dx > 0)? x++:x--){
            draw_char( x, y, value, pair_id );
            err += derr;

            while (err >= 0.5 && ((dy > 0)? y <= y2:y >= y2)){
                  draw_char( x, y, value, pair_id );                                                                         y += ( dy > 0 ) - ( dy < 0 );
                  err -= 1.0;                                                                                 }
         }
    }
}

void get_screen_size(int16_t *width, int16_t *height){
    *width = get_screen_width();
    *height = get_screen_height();
}

int16_t get_screen_width(void){
    return (!override_screen)? getmaxx(stdscr):override_screen->width;
}

int16_t get_screen_height(void){
    return (!override_screen)? getmaxy(stdscr):override_screen->height;
}

int16_t wait_char(void){
    timeout(-1);
    int16_t result = getch();
    timeout(0);
    return result;
}

int16_t get_char(void){
    return getch();
}

void override_screen_size(const int16_t width, const int16_t height){
    if (override_screen){
        free(override_screen->buffer);
        free(override_screen);
    }
    override_screen = calloc(1, sizeof(screen));
    override_screen->width = width;
    override_screen->height = height;
    override_screen->buffer = calloc(width * height, sizeof(char) );
    memset( override_screen->buffer, ' ', width * height );
}


