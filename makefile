# Makefile for simulation development kit (SDK)

CC=gcc
TARGET=libsdk.a
FLAGS=-c -Wall -Werror -std=gnu99

all: $(TARGET)

$(TARGET): obj/timers.o obj/sprites.o obj/colors.o obj/graphics.o
	ar r $(TARGET) obj/timers.o obj/sprites.o obj/graphics.o obj/colors.o

obj/timers.o: include/timers.h
	$(CC) $(FLAGS) -o obj/timers.o -I include src/timers.c

obj/colors.o: include/colors.h
	$(CC) $(FLAGS) -o obj/colors.o -I include src/colors.c

obj/graphics.o: include/graphics.h include/colors.h
	$(CC) $(FLAGS) -o obj/graphics.o -I include src/graphics.c

obj/sprites.o: include/sprites.h include/graphics.h
	$(CC) $(FLAGS) -o obj/sprites.o -I include src/sprites.c

clean:
	rm $(TARGET)
	rm obj/*.o


